#node.peculiar-logger.js

#Description

##Installation
	npm i peculiar-logger

##Module connection
	const Logger = require ('peculiar-logger');

##Creating a journal
	const Journal = Logger.journal();
	const journal = new Journal(source, path, output);

###Description of parameters
* parameter 'source':
	* purpose -> source of logging,
	* type -> String,
	* required -> true
* parametr 'path':
	* purpose -> specify the full path for saving the log file, а также as well as the name of the collection for mongoDB,
	* type -> Object,
	* required -> false,
	* default -> { file: './logs, db: `logs-${source}` }
* parametr 'output':
	* purpose -> indicates where to output logs, by default the output goes to the console and to the file,
	* type -> Object,
	* required -> false;
	* default -> { console: true, file: true, db: false }

###Example of creating an instance
	const Journal = Logger.journal();
	const test1 = new Journal( 'Test1', {file: `${__dirname}/logs`}, {console: true, file: false, db: false} );
	const test2 = new Journal( 'Test2', {file: `D:\\logs`, db: 'logs-test2'}, {console: true, file: true, db: true} );
	const test3 = new Journal( 'Test3', , {console: true, file: true, db: false} );
	const test4 = new Journal( 'Test4', {file: '../logs'} );
	const test5 = new Journal( 'Test4' );

##Logger parameters

###current parameters of the logger
* parametr 'mongoDB':
	* type -> Object (Mongoose),
	* default -> null,
	* set -> true,
	* get -> true
* parametr 'dirname':
	* purpose -> default directory,
	* type -> String,
	* default -> null,
	* set -> true,
	* get -> true
* parametr 'collection':
	* purpose -> default collection,
	* type -> String,
	* default -> null,
	* set -> true,
	* get -> true
* parametr 'output':
	* purpose -> default output,
	* type -> Object,
	* default -> {
        console: null,
        db: null,
        file: null
    },
	* set -> true,
	* get -> true	
* parametr 'colors':
	* purpose -> default colors,
	* type -> Object,
	* default -> {
        error: '\x1b[31m%s\x1b[0m',
		fatal: '\x1b[35m%s\x1b[0m',
        warn: '\x1b[33m%s\x1b[0m',
        info: '\x1b[36m%s\x1b[0m',
        trace: '\x1b[37m%s\x1b[0m',
        debug: '\x1b[32m%s\x1b[0m'
    },
	* set -> true,
	* get -> true	
* parametr 'journals':
	* purpose -> stores all log settings,
	* type -> Object,
	* set -> false,
	* get -> true
	
###Parameter setting

####Parameter setting 'mongoDB'
	Logger.set({mongoDB: mongoose});

####Parameter setting 'dirname'
	Logger.set({dirname: '../mylogs'});

####Parameter setting 'collection'
	Logger.set({collection: 'mylogs'});

####Parameter setting 'output'
	Logger.set( {output: {
			console: true,
			db: false,
			file: false
		}
	});		

####Parameter setting 'colors'
	Logger.set( {colors: {
			error: '\x1b[31m%s\x1b[0m',
			warn: '\x1b[33m%s\x1b[0m',
			info: '\x1b[36m%s\x1b[0m',
			trace: '\x1b[37m%s\x1b[0m',
			debug: '\x1b[32m%s\x1b[0m'
		}
	});	

####Parameter setting all
	Logger.set({
		mongoDB: mongoose,
		dirname: '../mylogs',
		collection: 'mylogs',
		output: {
			console: true,
			db: false,
			file: false
		},
		colors: {
			error: '\x1b[31m%s\x1b[0m',
			warn: '\x1b[33m%s\x1b[0m',
			info: '\x1b[36m%s\x1b[0m',
			trace: '\x1b[37m%s\x1b[0m',
			debug: '\x1b[32m%s\x1b[0m'
		}
	});
	
###Getting Parameters
	Logger.get();
	
###Getting the parameter
	Logger.get('mongoDB');
	Logger.get('dirname');
	Logger.get('collection');
	Logger.get('output');
	Logger.get('colors');
	Logger.get('journals');

##Output Format

###Description
The default format is as follows:
	=> [2018-8-24 14:23:43] [WARN] [test] [warn test] 	
Call Logger.format(['message', 'level' ,'date', 'source']) with the transfer of parameters in the array with the desired output order.
Options:
* 'message' - log message.
* 'level' - log level.
* 'date' - log date.
* 'source' - log source.

###Example of creating a new format
	Logger.format(['message', 'level' ,'date', 'source']);
	=> [warn test] [WARN] [2018-8-24 14:23:43] [test] 
	Logger.format(['source', 'level' ,'date', 'message']);
	=> [test] [WARN] [2018-8-24 14:23:43] [warn test]  
	Logger.format(['source', 'level', 'message']);
	=> [test] [WARN] [warn test]  
	Logger.format(['message']);
	=> [warn test]  

##Use of a logger
 
###Log levels
* error
	* purpose -> designed to output error,
	* output color -> red,
* fatal
	* purpose -> designed to output fatal,
	* output color -> purple,	
* warn
	* purpose -> designed to output warn,
	* output color -> orange,
* info
	* purpose -> designed for normal output,
	* output color -> blue,
* trace
	* purpose -> designed to output trace,
	* output color -> white,
* debug
	* purpose -> designed to output debug,
	* output color -> green	

###Example of using logging levels
	test1.error('Test1 error');
	test1.fatal('Test1 fatal');
	test1.warn('Test1 warn');
	test1.info('Test1 info');
	test1.trace('Test1 trace');
	test1.debug('Test1 debug');
 
###Special conclusion
A specific log can be displayed in its own way, regardless of global settings.
To do this, you must pass the second parameter. 

###Example of using special output
	test1.info('Start test nodefault', {file: true} );
	test1.error('Start test nodefault', {file: true, db: true, console: false} );
	test1.info('Start test nodefault', {file: false, db: false} );
 
Note: If any of the three parameters are not transmitted in the object.
That will use the global parameter

#Example
	const Logger = require ('peculiar-logger');
	const mongoose = require ('mongoose');

	const Journal = Logger.journal();
	const log = new Journal( 'Test', {file: `../logs`}, {console: true, file: false, db: false} );

	try {
		Logger.set({mongoDB: mongoose});
	} catch ( error ) {
		console.log (error);
	}
	

	log.info('Start test default');
	log.info('Start test nodefault', {file: true} );

	let x = Math.floor(Math.random() * (10 - 1 + 1)) + 1;

	if (x < 4) {
		log.warn('Result x < 4', {file: true, db: true} );
	} else if {
		log.info('Result x = 4');
	} else { 
		log.error('Result x > 4', {file: true, db: true});
	}

#License
ISC