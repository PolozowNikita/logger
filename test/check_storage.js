const Logger = require ('../index');
const Journal = Logger.journal();
const journal = new Journal('test');
const journal1 = new Journal('test1');
const journal2 = new Journal('test2');
const journal3 = new Journal('test3');

console.log('-----------------TEST 1---------------------');
let req1 = Logger.get();
console.log(req1);
console.log('--------------------------------------------');

console.log('-----------------TEST 2---------------------');
let req2 = Logger.get();
console.log(req2.storage.journals);
console.log('--------------------------------------------');

console.log('-----------------TEST 3---------------------');
let req3 = Logger.get('journals');
console.log(req3);    
console.log('--------------------------------------------');