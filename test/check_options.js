const Logger = require ('../index');
const Journal = Logger.journal();
const journal = new Journal('test');

function set1() {
    console.log('-----------------TEST 1---------------------');
    Logger.set( {mongoDB: {test: 'test'} } );
    console.log('--------------------------------------------');
}


function get1() {
    console.log('-----------------TEST 2---------------------');
    let req = Logger.get();
    console.log(req);
    console.log('--------------------------------------------');
}

function get2() {
    let req = Logger.get('mongoDB');
    console.log('-----------------TEST 3---------------------');
    console.log(req);
    console.log('--------------------------------------------');
}

function get3() {
    let req = Logger.get();
    console.log('-----------------TEST 4---------------------');
    console.log(req.options.mongoDB);
    console.log('--------------------------------------------');
}

set1();
get1();
get2();
get3();