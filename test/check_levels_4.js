const Logger = require ('../index');
const Journal = Logger.journal();
const journal = new Journal('test');

Logger.set({output: {console: true, file: false, db: false}})
Logger.set({dirname: '../logs/test'})

journal.info('info test');
journal.warn('warn test', {console: false, file: true});
journal.error('error test');
journal.fatal('fatal test');
journal.debug('debug test');
journal.trace('trace test');