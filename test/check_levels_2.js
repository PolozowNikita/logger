const Logger = require ('../index');

const Journal = Logger.journal();
const journal = new Journal('test');

journal.info('info test');
journal.warn('warn test');
journal.error('error test');
journal.fatal('fatal test');
journal.debug('debug test');
journal.trace('trace test');