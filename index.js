/** Main logger file*/
const options = require('./lib/options');

class Main {

    constructor () {}

    /**
    * Transfer log module.
    * You must create an instance of the module.
    */
    journal () {
        return require('./lib/journal');
    }

    /**
    * Function of setting the parameters of the logger.
    * @param {Object} params - logger parameters.
    */ 
    set (params) {
        try {
            options.setOptions(params);
        } catch ( error ) {
            throw new Error( error );
        }
    }

    /**
    * The function of obtaining the parameters of the logger.
    * Ability to call a function without a parameter. As a result, all parameters will be displayed
    * @param {Object} params - logger parameter.
    */  
    get (param) {
        let responce;

        try {
            responce = options.getOptions(param);
        } catch ( message ) {
            throw new Error( message );
        }
        return responce;
    }

    /**
    * Function for setting a new format for message output.
    * Default: ['date', 'level', 'source', 'message'].
    * @param {array} array - output fields: 'date', 'level', 'source', 'message'.
    */     
    format (array) {
        let newFormat= [];
        for (let i = 0; i < array.length; i++) {
            switch (array[i]) {
                case 'date':
                    for (let j = 0; j < newFormat.length; j++) {
                        if (newFormat[j] === array[i]) {
                            throw new Error('Peculiar-logger. The second parameter date.');
                        } 
                    }
                    newFormat[i] = array[i];
                break;
                case 'level':
                    for (let j = 0; j < newFormat.length; j++) {
                        if (newFormat[j] === array[i]) {
                            throw new Error('Peculiar-logger. The second parameter level.');
                        } 
                    }
                    newFormat[i] = array[i];
                break;
                case 'source':
                    for (let j = 0; j < newFormat.length; j++) {
                        if (newFormat[j] === array[i]) {
                            throw new Error('Peculiar-logger. The second parameter source.');
                        } 
                    }
                    newFormat[i] = array[i];
                break;
                case 'message':
                    for (let j = 0; j < newFormat.length; j++) {
                        if (newFormat[j] === array[i]) {
                            throw new Error('Peculiar-logger. The second parameter message.');
                        } 
                    }
                    newFormat[i] = array[i];
                break;
                default:
                    throw new Error('Peculiar-logger. The format does not have these fields.');
            }
        }
        options.setFormat(newFormat);
    }
}

module.exports = new Main();