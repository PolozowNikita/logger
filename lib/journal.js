/** Class representing a logging. */
const writer = require('./writer');
const options = require('./options');
const functions = require('./functions');

class Journal { 

   /**
    * Create a logging.
    * @param {String} source Source of logging -> 'Test'
    * @param {Object} path Path to save -> {file: '/logs/test', db: 'logs-test'}. Parameter may not be specified. The following settings will be used: {console: true, file: true, db: false}
    * @param {Object} output Log output -> {console: true, file: true, db: 'false'}. Parameter may not be specified. The following settings will be used: {file: '_dirname/logs/', db: 'logs'}
    */
    constructor (source, path, output) {
        //Check for the transmitted first parameter
        if (source) {
            this.source = source;
        } else {
            throw new Error('Peculiar-logger. No message source specified.');
        }    
        
        this.path = {
            file: null, 
            db: null 
        }

        //standard logger output
        this.output = {
            console: null,
            db: null,
            file: null
        }    

        //check for the second parameter
        if (path) {
            if (path.file) {
                this.path.file = `${path.file}/`;
                try {
                    functions.createDir(this.path.file);
                } catch (err) {
                    throw new Error(err);
                }
            } 
            if (path.db) {
                this.path.db = path.db;
            } 
        } 

        //check for the third parameter
        if (output) {
            if (typeof output.console === 'boolean') {
                this.output.console = output.console;
            } 

            if (typeof output.file === 'boolean') {
                this.output.file = output.file;
            } 

            if (typeof output.db === 'boolean') {
                this.output.db = output.db;
            }   
        }

        let store = options.getOptions('journals');
        store.push({source: this.source, path: this.path, output: this.output});
    }

   /**
    * Creating settings for displaying the received message.
    * @param {Object} recordProperty - accepted settings.
    */
    messageTemplate (recordProperty) {        
        let params = options.getOptions();
        recordProperty.eventDate = new Date().toLocaleString().replace(/T/, ' ').replace(/\..+/, '').toString();;
        recordProperty.date = new Date().toLocaleDateString().toString();
        recordProperty.source = this.source;
        recordProperty.path = {};
        recordProperty.record = '';
        for (let i = 0; i < params.storage.format.length; i++) {
            switch (params.storage.format[i]) {
                case 'date':
                    recordProperty.record += `[${recordProperty.eventDate}] `
                break;
                case 'level':
                    recordProperty.record += `[${recordProperty.level}] `
                break;
                case 'source':
                    recordProperty.record += `[${recordProperty.source}] `
                break;
                case 'message':
                    recordProperty.record += `[${recordProperty.message}] `
                break;
            }
        }
        
        //Determine the path to the file
        if (!this.path.file) {
            if (params.options.dirname) { 
                recordProperty.path.file = `${params.options.dirname}/`;
            } else {
                recordProperty.path.file = '../logs/';
                try {
                    functions.createDir('../logs/');
                } catch (err) {
                    throw new Error(err);
                } 
            }
        } else {
            recordProperty.path.file = this.path.file;
        }

        //Determine the path to the collection
        if (!this.path.db) {
            if (params.options.collection) { 
                recordProperty.path.db = params.options.collection;
            } else {
                recordProperty.path.db = `logs-${this.source}`;
            }
        } else {
            recordProperty.path.db = this.path.db;
        }

        //Verification of the transferred settings of the individual output
        if (!recordProperty.output) {
            recordProperty.output = {};
            if (this.output.console) {
                recordProperty.output.console = this.output.console;
            } else if ( params.options.output.console && typeof params.options.output.console === 'boolean' ) {
                recordProperty.output.console = params.options.output.console;
            } else {
                recordProperty.output.console = true;
            }

            if (this.output.file) {
                recordProperty.output.file = this.output.file;
            } else if ( params.options.output.file && typeof params.options.output.file === 'boolean' ) {
                recordProperty.output.file = params.options.output.file;
            } else {
                recordProperty.output.file = true;
            }

            if (this.output.db) {
                recordProperty.output.db = this.output.db;
            } else if ( params.options.output.db && typeof params.options.output.db === 'boolean' ) {
                recordProperty.output.db = params.options.output.db;
            } else {
                recordProperty.output.db = false;
            }    
        } else {
            //Checking the individual console output
            switch (recordProperty.output.console) { 
                
                case true:
                    recordProperty.output.console = true;
                break;

                case false:
                    recordProperty.output.console = false;
                break;

                default:
                    if (this.output.console) {
                        recordProperty.output.console = this.output.console;
                    } else if ( params.options.output.console && typeof params.options.output.console === 'boolean' ) {
                        recordProperty.output.console = params.options.output.console;
                    } else {
                        recordProperty.output.console = true;
                    }
                break;
            }

            //Verifying individual output to a file
            switch (recordProperty.output.file) {

                case true:
                    recordProperty.output.file = true;
                break;

                case false:
                    recordProperty.output.file = false;
                break;

                default:
                    if (this.output.file) {
                        recordProperty.output.file = this.output.file;
                    } else if ( params.options.output.file && typeof params.options.output.file === 'boolean' ) {
                        recordProperty.output.file = params.options.output.file;
                    } else {
                        recordProperty.output.file = true;
                    }
                break;
            }

            //Checking individual output to the database
            switch (recordProperty.output.db) {
  
                case true:
                    recordProperty.output.db = true;
                break;

                case false:
                    recordProperty.output.db = false;
                break;

                default:
                    if (this.output.db) {
                        recordProperty.output.db = this.output.db;
                    } else if ( params.options.output.db && typeof params.options.output.db === 'boolean' ) {
                        recordProperty.output.db = params.options.output.db;
                    } else {
                        recordProperty.output.db = false;
                    }  
                break;
            }
        }

        //Calling the recording function
        writer.write(recordProperty);
    }

   /**
    * The function for displaying the error message.
    * @param {string} message Message -> 'Test message'
    * @param {Object} output individual output -> {console: true, db: true, file: true}. Parameter may not be specified. Global settings will be used
    */
    error (message, output) {
        let color = options.getOptions('colors').error;
        if (!color) {
            color = '\x1b[31m%s\x1b[0m';
        }
        let recordProperty = {
            level: 'ERROR',
            color: color,
            message: message,
            output: output
        }

        this.messageTemplate(recordProperty);
    } 

   /**
    * The function for displaying the fatal message.
    * @param {string} message Message -> 'Test message'
    * @param {Object} output individual output -> {console: true, db: true, file: true}. Parameter may not be specified. Global settings will be used
    */
    fatal (message, output) {
        let color = options.getOptions('colors').fatal;
        if (!color) {
            color = '\x1b[35m%s\x1b[0m';
        }
        let recordProperty = {
            level: 'FATAL',
            color: color,
            message: message,
            output: output
        }

        this.messageTemplate(recordProperty);
    }     

   /**
    * The function for displaying the warning messages.
    * @param {string} message Message -> 'Test message'
    * @param {Object} output individual output -> {console: true, db: true, file: true}. Parameter may not be specified. Global settings will be used
    */
    warn (message, output) {
        let color = options.getOptions('colors').warn;
        if (!color) {
            color = '\x1b[33m%s\x1b[0m';
        }
        let recordProperty = {
            level: 'WARN',
            color: color,
            message: message,
            output: output
        }

        this.messageTemplate(recordProperty);
    } 

   /**
    * The function for displaying the info messages.
    * @param {string} message Message -> 'Test message'
    * @param {Object} output individual output -> {console: true, db: true, file: true}. Parameter may not be specified. Global settings will be used
    */
    info (message, output) {
        let color = options.getOptions('colors').info;
        if (!color) {
            color = '\x1b[36m%s\x1b[0m';
        }
        let recordProperty = {
            level: 'INFO',
            color: color,
            message: message,
            output: output
        }

        this.messageTemplate(recordProperty);
    } 

   /**
    * The function for displaying the trace messages.
    * @param {string} message Message -> 'Test message'
    * @param {Object} output individual output -> {console: true, db: true, file: true}. Parameter may not be specified. Global settings will be used
    */
    trace (message, output) {   
        let color = options.getOptions('colors').trace;
        if (!color) {
            color = '\x1b[37m%s\x1b[0m';
        }     
        let recordProperty = {
            level: 'TRACE',
            color: color,
            message: message,
            output: output
        }

        this.messageTemplate(recordProperty);
    } 

   /**
    * The function for displaying the debug messages.
    * @param {string} message Message -> 'Test message'
    * @param {Object} output individual output -> {console: true, db: true, file: true}. Parameter may not be specified. Global settings will be used
    */
    debug (message, output) {      
        let color = options.getOptions('colors').debug;
        if (!color) {
            color = '\x1b[32m%s\x1b[0m';
        }  
        let recordProperty = {
            level: 'DEBUG',
            color: color,
            message: message,
            output: output
        }

        this.messageTemplate(recordProperty);
    }     
}

module.exports = Journal;