const fs = require('fs');

class Functions {
    static createDir (path) {
        try {
            fs.readdirSync(path);
            return;
        } catch (readDir1) {
            let separator = '\\';
            let pathTable = path.split('\\');
            if (pathTable.length == 1) {
                pathTable = path.split('/');
                separator = '/'
            }
            if (pathTable[pathTable.length - 1] === '') {
                pathTable.splice(pathTable.length - 1, 1);
            }
            for (let i = 0; i < pathTable.length; i++) {
                let arr = [];
                if (i === 0) {
                    arr.push(pathTable[i]);
                } else {
                    arr = pathTable.slice(0, i + 1);
                }
                let dir = arr.join(separator);
                //check directory and create
                try {
                    fs.readdirSync(dir);
                } catch (readDir2) {
                    try {
                        fs.mkdirSync(dir);
                    } catch (mkdir) {
                        throw new Error(`Peculiar-logger. Error create directory. Error: ${mkdir}`);
                    }
                }
            }
            return;
        }
    }
}

module.exports = Functions;