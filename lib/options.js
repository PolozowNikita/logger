/** Logger Options */
const functions = require('./functions');
//Logger parameters
let options = {
    mongoDB: null,
    dirname: null,
    collection: null,
    output: {
        console: null,
        db: null,
        file: null
    },
    colors: {
        error: '\x1b[31m%s\x1b[0m',
        fatal: '\x1b[35m%s\x1b[0m',
        warn: '\x1b[33m%s\x1b[0m',
        info: '\x1b[36m%s\x1b[0m',
        trace: '\x1b[37m%s\x1b[0m',
        debug: '\x1b[32m%s\x1b[0m'        
    },
    error: false
}

//Logger storage
let storage = {
    journals: [],
    format: ['date', 'level', 'source', 'message']
}

/**
* Function of setting the parameters of the logger.
* @param {Object} params - logger parameters.
*/ 
exports.setOptions = function ( params ) { 
    for (let key in params) {
        if (options[key] !== undefined) {
            options[key] = params[key];
            //create dir
            if (key === 'dirname') {
                try {
                    functions.createDir(params[key]);
                } catch (err) {
                    throw new Error(err);
                }
            }
        } else {
            throw new Error('Peculiar-logger. There is no such parameter.');
        }
    }
    return;
};

/**
* The function of obtaining the parameters of the logger.
* @param {Object} params - logger parameter.
* Ability to call a function without a parameter. As a result, all parameters will be displayed
*/ 
exports.getOptions = function ( param ) {
    let responce = {
        options: options,
        storage: storage
    };
    if (!param) {
        return responce;
    } else {
        if (options[param]) {
            return options[param];
        } else if (storage[param]) {
            return storage[param];
        } else {
            return null;
        }
    }
};

/**
* Function for setting a new format for message output.
* @param {array} array - output fields.
*/ 
exports.setFormat = function ( array ) {
    storage.format = array;
    return;
};