/** Class representing a writer. */
const fs = require ('fs');
const options = require ('./options');
const schema = require ('./mongo/schema');

class Writer {

    constructor() {}

   /**
    * Function, distribution output of the transmitted message based on settings.
    * @param {Object} recordProperty - 
    * recordProperty = {
    *   source: 'Test', -> Message sourceИсточник сообщения
    *   level: 'INFO',  -> Message level (INFO, WARN, ERROR, DEBUG, TRACE),
    *   eventDate: 2018-01-01 00:01:20, -> Date and time of the message,
    *   date: 01.01.2018, -> Date of the message
    *   color: '\x1b[36m%s\x1b[0m', -> Output Color
    *   message: 'Successful server creation', -> Transmitted message
    *   record: '[2018-01-01 00:01:20] [INFO] [Test] - Successful server creation', -> Upgraded message, for output
    *   output: { -> individual output
    *       console: true, -> output to the console
    *       db: true, -> output to the DB
    *       file: true -> output to the file
    *   },
    *   path: { -> Paths to the file and bd collections to write a message
    *       file: '/logs/test', -> The path to the file
    *       db: 'log-Test' -> Collection name
    *   } 
    */    
    static write (recordProperty) {
        //Check for output to the console
        if (recordProperty.output.console) {
            this.writeConsole(recordProperty);
        }
        //check for output to the file
        if (recordProperty.output.file) {
            this.writeFile(recordProperty);
        }
        //check for output to the DB
        if (recordProperty.output.db) {
            this.writeDB(recordProperty)
                .catch(error => { throw new Error (error) });
        }
    }

    //The function of outputting a record to the console
    static writeConsole (recordProperty) {
        switch (recordProperty.level) {
            case 'INFO':
                console.info(recordProperty.color, recordProperty.record);
            break;                
            case 'ERROR':
                console.error(recordProperty.color, recordProperty.record);
            break;
            case 'WARN':
                console.warn(recordProperty.color, recordProperty.record);
            break;   
            case 'DEBUG':
                console.debug(recordProperty.color, recordProperty.record);
            break;  
            case 'TRACE':
                console.trace(recordProperty.color, recordProperty.record);
            break;                   
            default:
                console.log(recordProperty.color, recordProperty.record);
            break;
        }
    }

    //Function of saving a record to a file
    static writeFile (recordProperty) {
        let postfix = '\r\n';
        //adding record log to file
        fs.appendFile(`${recordProperty.path.file}${recordProperty.source}.log`, `${recordProperty.record}${postfix}`, (err) => {
            if (err) {
                throw new Error(`Peculiar-logger. Error append file. Error: ${err}`);
            }
        });   
    }

    //The function of storing records in the database
    static async writeDB (recordProperty) {
        let mongoDB;

        try {
            mongoDB = await options.getOptions('mongoDB');
        } catch ( message ) {
            throw new Error ( message );
        }
        
        if (mongoDB != null) {
            let Schema = mongoDB.Schema;
            let model = mongoDB.model(recordProperty.path.db, new Schema(schema));

            let record = {
                date: Date(recordProperty.date),
                source: recordProperty.source,
                level: recordProperty.level,
                message: recordProperty.message
            };
    
            try {
                await model.create(record);
                return;
            } catch ( error )  {
                throw new Error(`Peculiar-logger. An error occurred while adding an entry to the database: ${error}`);
            }
        } else {
            throw new Error ('Peculiar-logger. No instance mongoose detected.');
        }
    }
}

module.exports = Writer;

