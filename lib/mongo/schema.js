/** Collection scheme for mongoDB */
module.exports = {
    date: {
        type: Date,
        unique: false,
        required: true,
        index: false
    },
    source: {
        type: String,
        unique: false,
        required: true,
        index: false
    },
    level: {
        type: String,
        unique: false,
        required: true,
        index: false
    },
    message: {
        type: String,
        unique: false,
        required: true,
        index: false
    }
};